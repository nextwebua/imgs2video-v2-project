#!/bin/bash

if [[ $# -ne 2 ]]
then
    echo "Usage: $0 <config file> <monitoring_pid_id>"
    echo "Config file should be derived from config-sample.i2v"
    exit 1
fi
export IMGS2VIDEO_CFGFILE=$1 # exporting for routines in sub-shells
if ! [[ -f $IMGS2VIDEO_CFGFILE ]]
then
    echo "Cfg file $IMGS2VIDEO_CFGFILE does not exist"
    exit 1
fi

if ! [[ -f "configureServerVariables.i2v" ]]
then
    echo "configureServerVariables file not exists"
    exit 1
fi

PARENT_PID=$2
IS_DAEMON_RUNNING="fail"
DAEMON_TERMINATION_DATE=0
export CONFIG_STATUS="ok"
export FFMPEG_STATUS="ok"

source $IMGS2VIDEO_CFGFILE
source dashboard.sh $IMGS2VIDEO_CFGFILE
source hv_and_log_sync.sh
source configureServerVariables.i2v



function save_monitoring_state_as_json {
    echo "{
    \"NAME\": \"$NAME\",
    \"IS_DAEMON_RUNNING\": \"$IS_DAEMON_RUNNING\",
    \"DAEMON_TERMINATION_DATE\": \"$DAEMON_TERMINATION_DATE\",
    \"FFMPEG_STATUS\": \"$FFMPEG_STATUS\",
    \"CONFIG_STATUS\": \"$CONFIG_STATUS\",
    \"CAMERA_STATUS\": \"$CAMERA_STATUS\",
    \"CAMERA_OFFLINE_DATE\": \"$CAMERA_OFFLINE_DATE\",
    \"CAMERA_ONLINE_DATE\": \"$CAMERA_ONLINE_DATE\",
    \"TOTAL_SPACE_KB\": \"$TOTAL_SPACE_KB\",
    \"LEAVE_FREE_SPACE_KB\": \"$(calculate_leave_free_space_kb)\",
    \"REPORT_CREATED_AT\": \"$(date +"%s")\"
    }" | jq . > "$BASEPATH/$NAME/monitoring.json"
}

while true
do
  if  [[ -f "$BASEPATH/$NAME/runtime.json" ]]
  then
    for s in $(cat "$BASEPATH/$NAME/runtime.json" | jq -r "to_entries|map(\"\(.key)=\(.value|tostring)\")|.[]" ); do
#      $s
      echo $s
      export $s
    done

    if ps -p $PARENT_PID > /dev/null
    then
       IS_DAEMON_RUNNING="ok"
       DAEMON_TERMINATION_DATE=0
       # Do something knowing the pid exists, i.e. the process with $PID is running

     else
      if [[$IS_DAEMON_RUNNING == "ok"]]
      then
        IS_DAEMON_RUNNING="fail"
        DAEMON_TERMINATION_DATE=$(date +"%s")
        UTC_NOW=`date -u +%s`

        echo "[$UTC_NOW][`date`][application][runtime] Приложение экстренно закончило свою работу " >> "$LOG_DIR/monitoring.log"
       fi
    fi

     EXPORT_VARIABLES=$(./configValidate.sh $IMGS2VIDEO_CFGFILE no)
     for R_V in $EXPORT_VARIABLES
     do
       echo $R_V
       export $R_V
     done

     ./configValidate.sh $IMGS2VIDEO_CFGFILE yes

    save_monitoring_state_as_json
  fi
  echo "Sleep"
  sleep 30

done