INSTALL_DIR="$(pwd)"
RUN_ON_START_PATH="/etc/rc.local"
STARTUP_CODE="cd $INSTALL_DIR && ./initd force-restart"

if ! [[ -f $RUN_ON_START_PATH ]]
then
echo "Create startup file"
touch $RUN_ON_START_PATH
chmod +x $RUN_ON_START_PATH
cat << 'EOF' >> "$RUN_ON_START_PATH"
#!/bin/bash
EOF
fi

if ! grep -Fxq "$STARTUP_CODE" $RUN_ON_START_PATH
then
echo "$STARTUP_CODE" >> "$RUN_ON_START_PATH"
echo "Startup successfully installed to $RUN_ON_START_PATH"
fi