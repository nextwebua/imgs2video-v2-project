#!/bin/bash

if [[ "$TERM" =~ "screen".* ]]; then
  echo "You are in TMUX OR SCREEN session. You CAN NOT run software from them. Exit...."
  exit 1
fi

if [[ $# -ne 1 ]]
then
    echo "Usage: $0 <config file>"
    echo "Config file should be derived from config-sample.i2v"
    exit 1
fi
export IMGS2VIDEO_CFGFILE=$1 # exporting for routines in sub-shells
if ! [[ -f $IMGS2VIDEO_CFGFILE ]]
then
    echo "Cfg file $IMGS2VIDEO_CFGFILE does not exist"
    exit 1
fi


export CONFIGURE_SERVER_VARIABLES='configureServerVariables.i2v'
if ! [[ -f $CONFIGURE_SERVER_VARIABLES ]]
then
    echo "Server config file $CONFIGURE_SERVER_VARIABLES does not exist"
    exit 1
fi



source $IMGS2VIDEO_CFGFILE
source $CONFIGURE_SERVER_VARIABLES

source dashboard.sh $IMGS2VIDEO_CFGFILE



mkdir -p $IMGSDIR
mkdir -p $VIDEODIR
mkdir -p "$SRC_DIR/video_ms"
mkdir -p "$SRC_DIR/log"
mkdir -p "$SRC_DIR/video_request"
mkdir -p "$SRC_DIR/video_range"
mkdir -p $DAILY_VIDEO_DIR
mkdir -p $LOG_DIR

#create monitoring file if not exists
if ! [[ -f $LOG_DIR/monitoring.log ]]
then
    touch "$LOG_DIR/monitoring.log"
fi



function hourly {
# args:
# $1 dir with images for this hour
    DIR=$1
    HOUR=`basename $DIR`
    DATE=`basename \`dirname $DIR\``

    echo "Test what we have" >>  $LOG_DIR/runtime_debug.log
    ls $DIR >>  $LOG_DIR/runtime_debug.log

    `dirname $0`/remove_old.sh

    echo "After remove old" >>  $LOG_DIR/runtime_debug.log
    ls $DIR >>  $LOG_DIR/runtime_debug.log

    echo Gonna remove zero-sized downloaded images, if any

    echo "Gonna remove zero-sized downloaded images" >>  $LOG_DIR/runtime_debug.log
    find $DIR -maxdepth 1 -type f -size 0 -exec rm -vf {} \;

    ls $DIR >>  $LOG_DIR/runtime_debug.log
    echo find $DIR -maxdepth 1 -type f -size 0 -exec rm -vf {} \; >> $LOG_DIR/runtime_debug.log
    echo $DIR >> $LOG_DIR/runtime_debug.log

    echo "After clean old" >>  $LOG_DIR/runtime_debug.log
    ls $DIR >>  $LOG_DIR/runtime_debug.log

    echo Gonna assemble $DIR
    if [[ -z "`ls $DIR`" ]]
    then
        echo This dir is empty, removing it and skipping assembling
        rmdir $DIR
    else
        ASSEMBLE_LOGFILE=$LOG_DIR/assemble__${DATE}_${HOUR}.log
        `dirname $0`/make_hourly_video.sh $DIR &> $ASSEMBLE_LOGFILE
        if [[ $? -ne 0 ]]
        then
            if [[ -n "$NOTIF_EMAILS" ]]
            then
                echo "ERROR: make_hourly_video.sh $DIR failed"
                cat $ASSEMBLE_LOGFILE | mail -s "Hourly video assembling failed on $NAME" -a $ASSEMBLE_LOGFILE $NOTIF_EMAILS
            fi
        fi
    fi

    if [[ "$NOCAT" == "yes" ]]
    then
        echo Configured to omit concatenation, skipping
    else
        CAT_LOGFILE=$LOG_DIR/cat__${DATE}_${HOUR}.log

        echo "CAT LOG TEST"
        echo `dirname $0`/cat_lastday.sh >> $LOG_DIR/runtime.log

        `dirname $0`/cat_lastday.sh &> $CAT_LOGFILE
        if [[ $? != 0 ]]
        then
            echo "ERROR: cat_lastday.sh failed (DATE=$DATE, HOUR=$HOUR)"
            if [[ -n "$NOTIF_EMAILS" ]]
            then
              cat $CAT_LOGFILE | mail -s "Video concatenation failed on $NAME" -a $CAT_LOGFILE $NOTIF_EMAILS
            fi
        fi
        echo "Concatenation succeed."
        `dirname $0`/cat_all_days.sh # cat's accidentally skipped daily videos
    fi

    $AFTER_HOUR_PROC_HOOK
    if [[ $HOUR == 23 ]]
    then
        $DAILY_HOOK
    fi
    echo "Hourly job succeed."
}
PREV_LAP_SECOND='unknown'
PREV_LAP_MINUTE='unknown'
PREV_LAP_HOUR='unknown'
PREV_LAP_DAY='unknown'


if [[ "$HV_AND_LOG_SYNC" == "yes" ]]
then
    echo "Starting infinite hourly_video/ and log/ sync process in this daemon instance"
    source `dirname $0`/hv_and_log_sync.sh
    hv_and_log_sync &
    ms_sync &
fi

while true
do
    UTC_NOW=`date -u +%s`
    NOW=$(date +"%d-%m-%Y %H:%M:%S")
    DATE=`date +'%F %H %M%S'`
    DAY=`echo $DATE | awk '{ printf $1 }'`
    HOUR=`echo $DATE | awk '{ printf $2 }'`
    MINSEC=`echo $DATE | awk '{ printf $3 }'`
    SECOND=`echo $DATE | awk '{ printf $3 }' | cut -c 3-4`
    MINUTE=`echo $DATE | awk '{ printf $3 }' | cut -c 1-2`


    mkdir -p $IMGSDIR/$DAY/$HOUR
    mkdir -p "$SRC_DIR/video_ms/$DAY"
    FILENAME=$IMGSDIR/$DAY/$HOUR/${MINSEC}.jpg

    MSECOND=${SECOND#0}

    #Every Second Action
    if [[ $PREV_LAP_SECOND != 'unknown' ]] && [[ $PREV_LAP_SECOND != $SECOND ]] && [ $(($MSECOND%5)) == 0 ]
    then
        wget --dns-timeout=1 --connect-timeout=2 --read-timeout=2  $URL -O $FILENAME 2>&1
#        wget --timeout=2  $URL -O $FILENAME 2>&1

        if [[ $? -ne 0 ]]
        then
            if [[ $CAMERA_STATUS == "ok" ]]
            then
                echo "[$UTC_NOW][`date`][camera][status] Камера Оффлайн. Была онлайн:`date -d "@$CAMERA_ONLINE_DATE"`" >> "$LOG_DIR/monitoring.log"
            fi
            CAMERA_STATUS="fail"
            CAMERA_ONLINE_DATE=0
            CAMERA_OFFLINE_DATE=$(date +"%s")
#            echo "`date` - wget $FILENAME failed, surviving" >> $LOG_DIR/runtime.log >&2
            echo "`date` - wget $FILENAME failed, surviving" >> $LOG_DIR/runtime.log
            rm $FILENAME
            cp ./no_signal.jpg $FILENAME
#            convert $FILENAME  -gravity SouthEast -pointsize 22 -fill white -annotate +30+30  "$NOW" $FILENAME
#            convert $FILENAME  -background Khaki label:"$NOW" -gravity center -append $FILENAME
            convert -background '#000' -fill white -gravity center \
              -size 300x30 -font Noto-Mono caption:"$NOW" \
              $FILENAME  +swap -gravity NorthWest -type TrueColor -composite \
              $FILENAME
        else
            convert -background '#000' -fill white -gravity center \
              -size 300x30 -font Noto-Mono caption:"$NOW" \
              $FILENAME  +swap -gravity NorthWest -type TrueColor -composite \
              $FILENAME

            if [[ $CAMERA_STATUS == "fail" ]]
            then
                echo "[$UTC_NOW][`date`][camera][status] Камера онлайн" >> "$LOG_DIR/monitoring.log"
            fi

            CAMERA_STATUS="ok"
            CAMERA_ONLINE_DATE=$(date +"%s")
            CAMERA_OFFLINE_DATE=0
            echo "`date` - File ${FILENAME} successfully downloaded"  >> $LOG_DIR/runtime.log
        fi


        if ! [[ -f $FILENAME ]]
        then
            cp ./no_signal.jpg $FILENAME
            convert -background '#000' -fill white -gravity center \
              -size 300x30 -font Noto-Mono caption:"$NOW" \
              $FILENAME  +swap -gravity NorthWest -type TrueColor -composite \
              $FILENAME
        fi

    fi

    #Every Minute Action
    if [[ $PREV_LAP_MINUTE != 'unknown' ]] && [[ $PREV_LAP_MINUTE != $MINUTE ]]
    then
        source $IMGS2VIDEO_CFGFILE

        save_runtime_state_as_json

        sleep $((RANDOM % 30)) && ./minutes_video_request.sh &

#        #every 5 minutes
#        MNS=${MINUTE#0}
#        MSHS=${HOUR#0}
#        if [ $(($MNS%5)) == 0 ]
#        then
#            sleep $((RANDOM % 30)) && ./merge_minute_range_videos.sh &
#
#            echo "`date` - Run code every 5 minutes"  >> $LOG_DIR/runtime_minutes.log
#            MS_MINUTE="$MINUTE"
#            MS_HOUR="$HOUR"
#            MS_DATE=`date +"%Y-%m-%d"`
#
#            if [[ $MINUTE == "00" ]] && [[ $HOUR != "00" ]]
#            then
#                MS_MINUTE=60
#                MS_HOUR=$((MSHS-1))
#                #$(printf "%02d" ${MINUTE#0})
#            else
#                if [[ $MINUTE == "00" ]] && [[ $HOUR == "00" ]]
#                then
#                    #yesrerday
#                    MS_DATE=`date -d "yesterday" '+%Y-%m-%d'`
#                    MS_HOUR=23
#                    MS_MINUTE=60
#                fi
#            fi
#
#
#
#            MS_HOUR=$(printf "%02d" ${MS_HOUR#0})
#            MS_MINUTE=$(printf "%02d" ${MS_MINUTE#0})
#
#
#            echo ./make_minutes_video.sh "$IMGSDIR/$DAY" $MS_DATE $MS_HOUR $MS_MINUTE 5 >> $LOG_DIR/runtime_minutes.log
#            ./make_minutes_video.sh "$IMGSDIR/$DAY" $MS_DATE $MS_HOUR $MS_MINUTE 5 >> $LOG_DIR/runtime_minutes.log &
#
#            echo "-----" >> $LOG_DIR/runtime_minutes.log
#
#        fi
#        #echo "`date` - Truncate file $LOG_DIR/runtime.log" >> $LOG_DIR/runtime.log
#        #truncate --size=100M $LOG_DIR/runtime.log
    fi


#    if [[ $PREV_LAP_MINUTE != 'unknown' ]] && [[ $PREV_LAP_MINUTE != $MINUTE ]]
#    then
#        echo " `date` - Hour has ticked from $PREV_LAP_HOUR to $HOUR, launching video assembling" >> $LOG_DIR/runtime.log
#        HOURLY_LOGFILE=$LOG_DIR/hourly__${DAY}_${HOUR}.log
#        hourly $IMGSDIR/$PREV_LAP_DAY/$PREV_LAP_HOUR &> $HOURLY_LOGFILE &
#    fi

    #Every Hour Action
    if [[ $PREV_LAP_HOUR != 'unknown' ]] && [[ $PREV_LAP_HOUR != $HOUR ]]
    then
#        echo " `date` - Monitoring log sync" >> $LOG_DIR/runtime.log
#        sync_with_retry "$BASEPATH/$NAME/monitoring.log" "$DST_URI/monitoring.log" &

#       Add a sleep for 30 minutes and run command
        sleep $((RANDOM % 1800)) && sync_with_retry "$BASEPATH/$NAME/monitoring.json" "$DST_URI/monitoring.json" &

        if [[ -f "$FILENAME" ]]
        then
          sleep $((RANDOM % 1800)) && sync_with_retry "$FILENAME" "$DST_URI/range_image.jpg" &
#            rsync -e "$RSYNC_E" $RSYNC_OPTS --archive --partial --verbose "$FILENAME" "$DST_URI/range_image.jpg"
        fi

        echo " `date` - Hour has ticked from $PREV_LAP_HOUR to $HOUR, launching video assembling" >> $LOG_DIR/runtime.log
        HOURLY_LOGFILE=$LOG_DIR/hourly__${DAY}_${HOUR}.log
        hourly $IMGSDIR/$PREV_LAP_DAY/$PREV_LAP_HOUR &> $HOURLY_LOGFILE &

#        if [[ $HOUR -eq 23 || $HOUR -eq 12 ]]
#        then
#          if [[ -f "$FILENAME" ]]
#          then
#            sync_with_retry "$FILENAME" "$DST_URI/range_image.jpg" &
##            rsync -e "$RSYNC_E" $RSYNC_OPTS --archive --partial --verbose "$FILENAME" "$DST_URI/range_image.jpg"
#          fi
#        fi

        if [[ ${HOUR#0} -eq 23 ]]
        then
          LOG_DATE=$(date '+%Y-%m-%d')
          cp $LOG_DIR/runtime.log  $LOG_DIR/runtime_$LOG_DATE.log
          cp $LOG_DIR/runtime_debug.log  $LOG_DIR/runtime_debug_$LOG_DATE.log
          cp $LOG_DIR/monitoring.log $LOG_DIR/monitoring_$LOG_DATE.log

          truncate -s 0 $LOG_DIR/runtime.log
          truncate -s 0 $LOG_DIR/runtime_debug.log
          truncate -s 0 $LOG_DIR/monitoring.log
        fi
    fi

#    $AFTER_GET_IMAGE_HOOK
    sleep 1
    PREV_LAP_HOUR=$HOUR
    PREV_LAP_DAY=$DAY
    PREV_LAP_MINUTE=$MINUTE
    PREV_LAP_SECOND=$SECOND
done
