cmake_minimum_required(VERSION 2.8.4)
include(ExternalProject)

option(DEBUG "If debug build" YES)

list(APPEND CONFIGURE_OPTIONS --disable-shared --enable-static --enable-pic
    --disable-cli --prefix=${CMAKE_CURRENT_SOURCE_DIR}/install)
if (${DEBUG})
  list(APPEND CONFIGURE_OPTIONS --enable-debug --disable-asm)
endif(${DEBUG})

ExternalProject_Add(x264_local
    URL https://code.videolan.org/videolan/x264/-/archive/master/x264-master.tar.gz
    BUILD_IN_SOURCE 1
    CONFIGURE_COMMAND ./configure ${CONFIGURE_OPTIONS}
    BUILD_COMMAND make
    INSTALL_COMMAND make install
    )
