#!/bin/bash


if [[ -f "config.i2v" ]]
then
    CONFIG_PATH="config.i2v"
else
    CONFIG_PATH="config-sample.i2v"
fi

source $CONFIG_PATH

echo "Camera Folder name [Default: $NAME]"
read NEW_NAME
if [[ -n "$NEW_NAME" ]]
then
  echo "SCTT"
    NAME=$NEW_NAME
fi

echo "Webcam URL [Default: $URL]"
read NEW_URL
if [[ -z "$NEW_URL" ]]
then
    NEW_URL=$URL
fi

wget --connect-timeout=2 --read-timeout=5 $NEW_URL -O 'webcam_test.jpg' 2>&1

if [[ $? -ne 0 ]]
then
    echo "`date` - Webcam URL failed for $NEW_URL, please try again"
    rm 'webcam_test.jpg'
    exit 1
else
    rm 'webcam_test.jpg'
    echo "Webcam URL - OK"
fi

echo "Sleep per every image in seconds [Default: $AFTER_GET_IMAGE_HOOK]"
read NEW_AFTER_GET_IMAGE_HOOK
if [[ -z "$NEW_AFTER_GET_IMAGE_HOOK" ]]
then
    NEW_AFTER_GET_IMAGE_HOOK=$AFTER_GET_IMAGE_HOOK
else
    NEW_AFTER_GET_IMAGE_HOOK="sleep $NEW_AFTER_GET_IMAGE_HOOK"
fi

echo "Notification Email [Default: $NOTIF_EMAILS]"
read NEW_NOTIF_EMAILS
if [[ -z "$NEW_NOTIF_EMAILS" ]]
then
    NEW_NOTIF_EMAILS=$NOTIF_EMAILS
fi


echo "Save IMAGE in days [Default: $SAVE_IMGS_DAYS]"
read NEW_SAVE_IMGS_DAYS
if [[ -z "$NEW_SAVE_IMGS_DAYS" ]]
then
    NEW_SAVE_IMGS_DAYS=$SAVE_IMGS_DAYS
fi


echo "Save Video HOURS in days [Default: $SAVE_VIDEO_HOURS_DAYS]"
read NEW_SAVE_VIDEO_HOURS_DAYS
if [[ -z "$NEW_SAVE_VIDEO_HOURS_DAYS" ]]
then
    NEW_SAVE_VIDEO_HOURS_DAYS=$SAVE_VIDEO_HOURS_DAYS
fi


echo "Save Video DAYS in days [Default: $SAVE_VIDEO_DAYS_DAYS]"
read NEW_SAVE_VIDEO_DAYS_DAYS
if [[ -z "$NEW_SAVE_VIDEO_DAYS_DAYS" ]]
then
    NEW_SAVE_VIDEO_DAYS_DAYS=$SAVE_VIDEO_DAYS_DAYS
fi


echo "Save LOGS in days [Default: $SAVE_LOG_DAYS]"
read NEW_SAVE_LOG_DAYS
if [[ -z "$NEW_SAVE_LOG_DAYS" ]]
then
    NEW_SAVE_LOG_DAYS=$SAVE_LOG_DAYS
fi

source configureVariables.i2v
source configureServerVariables.i2v

#echo "Name: $NEW_NAME"
#echo "Assets Path: $NEW_BASEPATH"
#echo "Notification Email: $NEW_NOTIF_EMAILS"
#echo "Webcam URL: $NEW_URL"

echo "Configuration done"


echo "NAME=\"$NAME\"
BASEPATH=\"$(pwd)\"

FFMPEG=$FFMPEG
FFPROBE=$FFPROBE
BITRATE=$BITRATE
SPEEDUP=$SPEEDUP
FRAMERATE=$FRAMERATE
#declare -A VIDEO_ENCODING_OPTS_ARRAY
VIDEO_ENCODING_OPTS_ARRAY[\"mp4\"]=\" -t 60 -vcodec libx264 -b:v \$BITRATE -bt:v \$(( BITRATE / 10 )) -preset medium -movflags faststart \"
#VIDEO_ENCODING_OPTS_ARRAY[\"webm\"]=\" -t 60 -vcodec vp8 -b:v \$BITRATE -bt:v \$(( BITRATE / 10 )) \"
FILTER=\"$FILTER\"
OFMTS=\"$OFMTS\" # output format
#NOCAT=yes # uncomment to omit concatenation and make only hourly video fragments

IMGSDIR=\"$IMGSDIR\"
VIDEODIR=\"$VIDEODIR\"
DAILY_VIDEO_DIR=\"$DAILY_VIDEO_DIR\"
LOG_DIR=\"$LOG_DIR\"
SAVE_IMGS_DAYS=$NEW_SAVE_IMGS_DAYS
SAVE_VIDEO_HOURS_DAYS=$NEW_SAVE_VIDEO_HOURS_DAYS
SAVE_VIDEO_DAYS_DAYS=$NEW_SAVE_VIDEO_DAYS_DAYS
SAVE_LOG_DAYS=$NEW_SAVE_LOG_DAYS
DAYFILE=\"$DAYFILE\" #prefix without extension
URL=\"$NEW_URL\"
# one more nice URL: http://www.wirednewyork.com/images/webcams/wired-new-york-webcam3.jpg

AFTER_GET_IMAGE_HOOK='$NEW_AFTER_GET_IMAGE_HOOK'
AFTER_HOUR_PROC_HOOK='$AFTER_HOUR_PROC_HOOK'
DAILY_HOOK='$DAILY_HOOK'
NOTIF_EMAILS=\"$NEW_NOTIF_EMAILS\"" > config.i2v

nano config.i2v

echo "Do you want save file to the remote server ? [yes/no]"
read IS_SAVE
if [[ "$IS_SAVE" == "yes" ]]
then
  rsync -e "$RSYNC_E" $RSYNC_OPTS --archive --partial --verbose "$BASEPATH/config.i2v" "$DST_URI/"

  if [[ $? -ne 0 ]]
  then
      echo "Config File NOT UPLOADED TO REMOTE SERVER"
      exit 1
  else
      echo "Config File SUCCESSFULLY UPLOADED TO REMOTE SERVER"
  fi
fi