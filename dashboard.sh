#!/bin/bash

if [[ $# -ne 1 ]]
then
    echo "Usage: $0 <config file>"
    echo "Config file should be derived from config-sample.i2v"
    exit 1
fi
export IMGS2VIDEO_CFGFILE=$1 # exporting for routines in sub-shells
if ! [[ -f $IMGS2VIDEO_CFGFILE ]]
then
    echo "Cfg file $IMGS2VIDEO_CFGFILE does not exist"
    exit 1
fi

source $IMGS2VIDEO_CFGFILE

function calculate_leave_free_space_kb {
    df -k "$BASEPATH/$NAME"| tail -1 | awk '{print $4}' 
}

function save_runtime_state_as_json {
    echo "{
    \"NAME\": \"$NAME\",
    \"CAMERA_STATUS\": \"$CAMERA_STATUS\",
    \"CAMERA_OFFLINE_DATE\": \"$CAMERA_OFFLINE_DATE\",
    \"CAMERA_ONLINE_DATE\": \"$CAMERA_ONLINE_DATE\",
    \"TOTAL_SPACE_KB\": \"$TOTAL_SPACE_KB\",
    \"LEAVE_FREE_SPACE_KB\": \"$(calculate_leave_free_space_kb)\",
    \"REPORT_CREATED_AT\": \"$(date +"%s")\"
    }" | jq . > "$BASEPATH/$NAME/runtime.json"
}

CAMERA_STATUS="ok"
CAMERA_OFFLINE_DATE=0
CAMERA_ONLINE_DATE=0
LAST_REMOTE_CONNECTION_STATUS="ok"


TOTAL_SPACE_KB=$(df -k "$BASEPATH/$NAME"| tail -1 | awk '{print $2}')
LEAVE_FREE_SPACE_KB=$(calculate_leave_free_space_kb)



echo $TOTAL_SPACE_KB
echo $LEAVE_FREE_SPACE_KB

