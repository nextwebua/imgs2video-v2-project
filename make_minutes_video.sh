#!/bin/bash

if [[ $# -ne 4 ]]
then
    echo "Usage: $0 <dir with hour images> <date|2020-05-05> <hour{00-23}> <minute{05-60} 05 means 00-05, 60 means 55-60>"
    exit 1
fi

if [[ -z $IMGS2VIDEO_CFGFILE ]]
then
    echo 'IMGS2VIDEO_CFGFILE env var must be available'
    exit 1
fi
if ! [[ -f $IMGS2VIDEO_CFGFILE ]]
then
    echo "Cfg file $IMGS2VIDEO_CFGFILE does not exist"
    exit 1
fi
source $IMGS2VIDEO_CFGFILE


VIDEO_MINUTES_FRAMERATE=30

DIR=$1
DATE=$2
HOUR=$3
END_MINUTE_HUMAN=$4
VIDEO_DURATION=$5
END_MINUTE=$((END_MINUTE_HUMAN-1))
START_MINUTE=$((END_MINUTE_HUMAN-VIDEO_DURATION))
FILE_FORMAT=".jpg"

if [ ! -d "$DIR" ]
then
    UTC_NOW=`date -u +%s`
    echo "[$UTC_NOW][`date`][video][minutes] Папка для создания минутных видео не существует - $DIR"
    echo "[$UTC_NOW][`date`][video][minutes] Папка для создания минутных видео не существует - $DIR" >> "$LOG_DIR/monitoring.log"
    exit 1
fi

OUTPUT_FOLDER="$BASEPATH/$NAME/video_ms/$DATE"
OUTPUT_FILE_PATH="$OUTPUT_FOLDER/${DATE}__${HOUR}__${END_MINUTE_HUMAN}.mp4"

mkdir -p $OUTPUT_FOLDER

FFMPEG_SOURCE_FILE="$BASEPATH/$NAME/minutes_ffmpeg_source.txt"
#recreate file
if [ -f $FFMPEG_SOURCE_FILE ]
then
    rm $FFMPEG_SOURCE_FILE
fi
touch $FFMPEG_SOURCE_FILE


echo "Prepare file for ffmpeg - $FFMPEG_SOURCE_FILE"

for MINUTE in $(seq "$START_MINUTE" "$END_MINUTE")
do
    #at the end #0 for fractal to keep numbers like 09 as variable
    FORMAT_MINUTE=$(printf "%02d" ${MINUTE#0})
    # echo $minuteFile
    for SECOND in {00..59}
    do
        #at the end #0 for fractal to keep numbers like 09 as variable
        FORMAT_SECOND=$(printf "%02d" ${SECOND#0})
        FILE=$DIR/$HOUR/$FORMAT_MINUTE$FORMAT_SECOND$FILE_FORMAT
        echo $FILE

        if [ -f $FILE ]
        then
            echo "file '$FILE'" >> $FFMPEG_SOURCE_FILE
        fi
    done
done



if [[ -z $(grep '[^[:space:]]' $FFMPEG_SOURCE_FILE) ]]
then
    echo "Images not found..."
    exit 1
fi


FFMPEG_RUN_STATUS="fail"
$FFMPEG \
    -f concat \
    -safe 0 \
    -i $FFMPEG_SOURCE_FILE \
    -c:v libx264 \
    -vf "fps=$VIDEO_MINUTES_FRAMERATE,format=yuv420p" \
    -y $OUTPUT_FILE_PATH && FFMPEG_RUN_STATUS="ok"

if [[ $FFMPEG_RUN_STATUS == "" ]]
then
    FFMPEG_RUN_STATUS="fail"
fi

if [[ $FFMPEG_RUN_STATUS == "ok" ]]
then
    echo "FILE Was Successfully Saved TO $OUTPUT_FILE_PATH"
else
    echo "ERROR - FILE $OUTPUT_FILE_PATH was note generate via FFMPEG"
fi


#./buildlib/ffmpeg -f concat -safe 0 -i test-image/minutes_ffmpeg_source.txt -c:v libx264 -vf "fps=25,format=yuv420p" ffmpeg_framerate_25.mp4