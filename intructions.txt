Software installation guide:
1. Install wget command (can be skipped on new Ubuntu versions)
sudo apt-get install wget

2. Install img2video
sudo wget -O - https://craneship.ua/theme/index/firmware/install/install.sh | sudo bash

3. Configure your img2video
Run command:
sudo ./initd configure
- Please answer yes for question: Do you want save file to the remote server ? [yes/no]

4. Configure remote connection with server (for rsync video to remote server)
Note: Can be avoid in case default access key installed on system level
Download user access key for server
Change path to access key in configureServerVariables.i2v file
Example:
- RSYNC_E="ssh -i ~/.ssh/id_rsa -p $MASTER_SERVER_SSH_PORT -o ConnectTimeout=10"
where -i ~/.ssh/id_rsa - it's a path to access file

There you can also change different rsync options like:
- MASTER_SERVER - IP of remote server (can be domain name)
- RSYNC_OPTS="--timeout 10" - it's a connection timeout in seconds
- DST_URI="netxweb@$MASTER_SERVER:/opt/i2v/$NAME" - URL TO copy in remote server (be careful - cron use this url /opt/i2v/)

You are set !!!




-------------------
Additional information for rebuild ffmpeg via docker
commands:
- Install docker
Run commands:
- docker build -f Docker.dockerfile -t imgs2video --no-cache .
- docker run -d imgs2video

Then download new version of ffmpeg
to copy build from docker - docker cp 1300454be2e1:/var/www/html/ffprobe .
