#!/bin/bash

declare -A SYNC_LIST

RSYNC_LIST_FILE="$SRC_DIR/hv_sync.list"

RSYNC_MS_FILE="$SRC_DIR/ms_sync.list"


RSYNC_OPTS="$RSYNC_OPTS --size-only --partial --partial-dir=.rsync-partial"


function hv_and_log_sync {
    if [[ -z "$DST_URI" ]]
    then
	    echo "DST_URI not set, aborting" >&2
	    exit
    fi


    while true
    do
        sleep 60  # give some rest to weak net channels
        # Get a list of files to sync in video_hours/ with --dry-run, on failure `continue` (go to next lap)
        # Sort this list, most recent modification time first
        # Used naming scheme allows simple lexicographical sorting to work as mtime sort
        touch "${RSYNC_LIST_FILE}.timestamp"
        sleep $((RANDOM % 600))


        rsync --bwlimit=1000 -e "$RSYNC_E" $RSYNC_OPTS --archive --dry-run --verbose "$SRC_DIR/video_hours" "$DST_URI" | egrep 'video_hours/.+' | sort --reverse > "$RSYNC_LIST_FILE"


        if [[ $? != 0 ]]
        then
            continue
        fi

        if [[ `wc -l "$RSYNC_LIST_FILE" | awk '{ print $1 }'` == 0 ]]
        then
            while [[ `find "$SRC_DIR/video_hours" -maxdepth 0 -newer "${RSYNC_LIST_FILE}.timestamp" | wc -l` == 0 ]]
            do
                sleep $((RANDOM % 600))
            done

            sync_with_retry "${RSYNC_LIST_FILE}.timestamp" "$DST_URI/hv_and_sync"

            continue
        fi

        # Start with the most recent video_hours/ file.
        I=1
        MOST_RECENT_VIDEO_HOURS_FILE="$SRC_DIR/`head -n 1 $RSYNC_LIST_FILE`"
        while true
        do
            CURRENT_FILE=`tail -n +$I $RSYNC_LIST_FILE | head -n 1` # TODO Get i-th file in the list
            # If all entries are done, upload logs
            if [[ "$CURRENT_FILE" == "" ]]
            then
                break
            fi

            I=$(( I + 1 ))

            # rsync this file alone (look out for correct destination dirs)
            RET=1
            while true
            do
                sleep $((RANDOM % 600))
                echo "Start real rsync command"
                rsync --bwlimit=1000 -e "$RSYNC_E" $RSYNC_OPTS --archive --partial --verbose --rsync-path="ionice -c2 -n7 rsync" "$SRC_DIR/$CURRENT_FILE" "$DST_URI/video_hours/"

                # On success move on to next file;
                if [[ $? == 0 ]]
                then
                    LAST_REMOTE_CONNECTION_STATUS="ok"
                    break  # go to next $CURRENT_FILE
                else
                    if [[ $LAST_REMOTE_CONNECTION_STATUS == "ok"  ]]
                    then
                        LAST_REMOTE_CONNECTION_STATUS="fail"
                        UTC_NOW=`date -u +%s`

                        echo "[$UTC_NOW][`date`][server][connection] Таймаут подключения к серверу" >> "$LOG_DIR/monitoring.log"
                    fi

                fi

                # ...If newer file appears in video_hours (the dir itself gets modified, as checked here), rebuild the list and start from the beginning
		# Also restart if $MOST_RECENT_VIDEO_HOURS_FILE disappears
                if [[ ! -e $MOST_RECENT_VIDEO_HOURS_FILE ]] \
			|| [[ `find "$SRC_DIR/video_hours" -maxdepth 0 -newer $MOST_RECENT_VIDEO_HOURS_FILE | wc -l` != 0 ]]
                then
                    break 2
                fi
            done
        done

        # Sync log/
        # Retry only if rsync exited with failure and video_hours/ doesn't have newer files
        while true
        do
            sleep $((RANDOM % 600))
            rsync --bwlimit=1000 -e "$RSYNC_E" $RSYNC_OPTS --archive --partial --verbose --rsync-path="ionice -c2 -n7 rsync" "$SRC_DIR/log" "$DST_URI"
            if [[ $? == 0 ]] || [[ `find "$SRC_DIR/video_hours" -maxdepth 0 -newer $MOST_RECENT_VIDEO_HOURS_FILE | wc -l` != 0 ]]
            then
                break
            fi
        done
    done
}



function ms_sync {
    if [[ -z "$DST_URI" ]]
    then
	    echo "DST_URI not set, aborting" >&2
	    exit
    fi

    while true
    do
        sleep 60  # give some rest to weak net channels
        # Get a list of files to sync in video_hours/ with --dry-run, on failure `continue` (go to next lap)
        # Sort this list, most recent modification time first
        # Used naming scheme allows simple lexicographical sorting to work as mtime sort
        touch "${RSYNC_MS_FILE}.timestamp"
        sleep $((RANDOM % 600))
        rsync --bwlimit=1000 -e "$RSYNC_E" $RSYNC_OPTS --archive --dry-run --verbose --rsync-path="ionice -c2 -n7 rsync" "$SRC_DIR/video_range" "$DST_URI" | egrep 'video_range/.+' | sort --reverse > "$RSYNC_MS_FILE"
        if [[ $? != 0 ]]
        then
            continue
        fi

        if [[ `wc -l "$RSYNC_MS_FILE" | awk '{ print $1 }'` == 0 ]]
        then
            while [[ `find "$SRC_DIR/video_range" -maxdepth 0 -newer "${RSYNC_MS_FILE}.timestamp" | wc -l` == 0 ]]
            do
                sleep $((RANDOM % 600))
            done

            continue
        fi

        # Start with the most recent video_hours/ file.
        I=1
        MOST_RECENT_VIDEO_MS_FILE="$SRC_DIR/`head -n 1 $RSYNC_MS_FILE`"
        while true
        do
            CURRENT_FILE=`tail -n +$I $RSYNC_MS_FILE | head -n 1` # TODO Get i-th file in the list
            # If all entries are done, upload logs
            if [[ "$CURRENT_FILE" == "" ]]
            then
                break
            fi

            I=$(( I + 1 ))

            # rsync this file alone (look out for correct destination dirs)
            RET=1
            while true
            do
                sleep $((RANDOM % 600))
                rsync --bwlimit=1000 -e "$RSYNC_E" $RSYNC_OPTS --archive --partial --verbose --rsync-path="ionice -c2 -n7 rsync" "$SRC_DIR/$CURRENT_FILE" "$DST_URI/video_range/"
                # On success move on to next file;
                if [[ $? == 0 ]]
                then
                    LAST_REMOTE_CONNECTION_STATUS="ok"
                    break  # go to next $CURRENT_FILE
                else
                    if [[ $LAST_REMOTE_CONNECTION_STATUS == "ok"  ]]
                    then
                        LAST_REMOTE_CONNECTION_STATUS="fail"
                        UTC_NOW=`date -u +%s`
                        echo "[$UTC_NOW][`date`][server][connection] Таймаут подключения к серверу" >> "$LOG_DIR/monitoring.log"
                    fi

                fi

                # ...If newer file appears in video_range (the dir itself gets modified, as checked here), rebuild the list and start from the beginning
		# Also restart if $MOST_RECENT_VIDEO_MS_FILE disappears
                if [[ ! -e $MOST_RECENT_VIDEO_MS_FILE ]] \
			|| [[ `find "$SRC_DIR/video_range" -maxdepth 0 -newer $MOST_RECENT_VIDEO_MS_FILE | wc -l` != 0 ]]
                then
                    break 2
                fi
            done
        done
    done
}

function sync_with_retry {
  local CP_FROM="$1"
  local CP_TO="$2"
#  local CNT=0
  if ! [ ${SYNC_LIST[$CP_TO]+_} ]
  then 

    SYNC_LIST[$CP_TO]=$CP_TO
    while true
    do
#       sleep 1-55 seconds
        sleep $((RANDOM % 55))
        rsync -e "$RSYNC_E" --rsync-path="ionice -c2 -n7 rsync" --archive --partial  "$CP_FROM" "$CP_TO"
        if [[ $? == 0 ]]
        then
            unset SYNC_LIST[$CP_TO]
            break
        fi

#        CNT=$((CNT+1))
#        if [[ $CNT == 5 ]]
#        then
#            break
#        fi
    done
  fi
}