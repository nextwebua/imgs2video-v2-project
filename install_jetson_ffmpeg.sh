#!/bin/bash

SOFTWARE_DIR="$(pwd)"

./initd stop

#build
git clone https://github.com/jocover/jetson-ffmpeg.git
cd jetson-ffmpeg
mkdir build
cd build
cmake ..
make
sudo make install
sudo ldconfig

#patch
git clone git://source.ffmpeg.org/ffmpeg.git -b release/4.2 --depth=1
cd ffmpeg
wget https://github.com/jocover/jetson-ffmpeg/raw/master/ffmpeg_nvmpi.patch
git apply ffmpeg_nvmpi.patch
./configure --enable-nvmpi
make

#build check
echo "Build check"
chmod 777 ./ffmpeg
chmod 777 ./ffprobe
FFMPEG=./ffmpeg
if ! [[ -f $FFMPEG ]]
then
    echo "Build fail, please check logs"
    exit 1
fi



#replace
rm -rf $SOFTWARE_DIR/buildlib/*
echo " Debug $SOFTWARE_DIR/jetson-ffmpeg/build/ffmpeg/ffmpeg $SOFTWARE_DIR/buildlib/ffmpeg"
ln -s $SOFTWARE_DIR/jetson-ffmpeg/build/ffmpeg/ffmpeg $SOFTWARE_DIR/buildlib/ffmpeg
ln -s $SOFTWARE_DIR/jetson-ffmpeg/build/ffmpeg/ffprobe $SOFTWARE_DIR/buildlib/ffprobe
chmod 777 $SOFTWARE_DIR/buildlib/ffmpeg
chmod 777 $SOFTWARE_DIR/buildlib/ffprobe
echo "Please replace codecs in ffmpeg to h264_nvmpi"
echo "Supports Encoding H.264/AVC and HEVC, for more info please visit link - https://github.com/jocover/jetson-ffmpeg"
echo "ffmpeg -c:v h264_nvmpi -i input_file"

