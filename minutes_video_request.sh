#!/bin/bash
if [[ -z $IMGS2VIDEO_CFGFILE ]]
then
    echo 'IMGS2VIDEO_CFGFILE env var must be available'
    exit 1
fi
if ! [[ -f $IMGS2VIDEO_CFGFILE ]]
then
    echo "Cfg file $IMGS2VIDEO_CFGFILE does not exist"
    exit 1
fi
source $IMGS2VIDEO_CFGFILE

DOMAIN_NAME="http://i2v.transship.ua"
JSON_FILE_PATH=$BASEPATH/$NAME/video_range.json
echo $DOMAIN_NAME/api/video/getAnyRange/$NAME
wget --connect-timeout=5 --read-timeout=5 --tries=5 $DOMAIN_NAME/api/video/getAnyRange/$NAME -O $JSON_FILE_PATH 2>&1
if [[ $? -ne 0 ]]
then
    echo "Server not available"
    exit 1
else
    JSON_CONTENT=$(cat $JSON_FILE_PATH)
fi

echo "CONTENT"
echo $JSON_CONTENT
#json content for test
# JSON_CONTENT=$(cat video_event.json)

if [ $JSON_CONTENT == "[]" ]
then
    echo "No event"
    exit 1
fi

VIDEO_MINUTES_FRAMERATE=30

VIDEO_ID=$(echo $JSON_CONTENT | jq -r '.id')
DATE_FOLDER=$(echo $JSON_CONTENT | jq -r '.date')
HOUR=$(echo $JSON_CONTENT | jq -r '.hour')
START_MINUTE=$(echo $JSON_CONTENT | jq -r '.startMinute')
END_MINUTE=$(echo $JSON_CONTENT | jq -r '.endMinute')
FILE_FORMAT=".jpg"


DIR="$IMGSDIR/$DATE_FOLDER"

MERGE_OUTPUT_FOLDER="$BASEPATH/$NAME/video_range"
OUTPUT_FILE_PATH="$MERGE_OUTPUT_FOLDER/$VIDEO_ID.mp4"

mkdir -p $MERGE_OUTPUT_FOLDER

FFMPEG_SOURCE_FILE="$BASEPATH/$NAME/minutes_ffmpeg_source.txt"
#recreate file
if [ -f $FFMPEG_SOURCE_FILE ]
then
    rm $FFMPEG_SOURCE_FILE
fi
touch $FFMPEG_SOURCE_FILE

echo "Prepare file for ffmpeg - $FFMPEG_SOURCE_FILE"

for MINUTE in $(seq "$START_MINUTE" "$END_MINUTE")
do
    #at the end #0 for fractal to keep numbers like 09 as variable
    FORMAT_MINUTE=$(printf "%02d" ${MINUTE#0})
    # echo $minuteFile
    for SECOND in {00..59}
    do
        #at the end #0 for fractal to keep numbers like 09 as variable
        FORMAT_SECOND=$(printf "%02d" ${SECOND#0})
        FILE=$DIR/$HOUR/$FORMAT_MINUTE$FORMAT_SECOND$FILE_FORMAT
        echo $FILE

        if [ -f $FILE ]
        then
            echo "file '$FILE'" >> $FFMPEG_SOURCE_FILE
        fi
    done
done

if [[ -z $(grep '[^[:space:]]' $FFMPEG_SOURCE_FILE) ]]
then
    wget --connect-timeout=5 --read-timeout=5 --tries=5 $DOMAIN_NAME/api/video/anyRangeResponse/$VIDEO_ID/NOT_FOUND -O /dev/null 2>&1
    #TODO - send curl
    exit 1
fi


FFMPEG_RUN_STATUS="fail"
$FFMPEG \
    -f concat \
    -safe 0 \
    -i $FFMPEG_SOURCE_FILE \
    -c:v libx264 \
    -vf "fps=$VIDEO_MINUTES_FRAMERATE,format=yuv420p" \
    -y $OUTPUT_FILE_PATH && FFMPEG_RUN_STATUS="ok"

if [[ $FFMPEG_RUN_STATUS == "" ]]
then
    FFMPEG_RUN_STATUS="fail"
fi

if [[ $FFMPEG_RUN_STATUS == "ok" ]]
then
    echo "FILE Was Successfully Saved TO $OUTPUT_FILE_PATH"
    wget --connect-timeout=5 --read-timeout=5 --tries=5 $DOMAIN_NAME/api/video/anyRangeResponse/$VIDEO_ID/OK -O /dev/null 2>&1
else
    echo "ERROR - FILE $OUTPUT_FILE_PATH was note generate via FFMPEG"
    wget --connect-timeout=5 --read-timeout=5 --tries=5 $DOMAIN_NAME/api/video/anyRangeResponse/$VIDEO_ID/FFMPEG_ERROR -O /dev/null 2>&1
fi

