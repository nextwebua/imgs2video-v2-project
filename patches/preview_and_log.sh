#!/bin/bash

CLIENT_CONFIG_FOLDER="client_config"
CLIENT_CONFIG_FILES="${CLIENT_CONFIG_FOLDER}/*.i2v"

./initd stop

#get Server Path
if [[ -f "configureServerVariables.i2v" ]]
then
    CONFIG_SERVER_PATH="configureServerVariables.i2v"
else
    CONFIG_SERVER_PATH="configureServerVariablesSample.i2v"
fi

source $CONFIG_SERVER_PATH




#update latest files daemon.sh and configure.sh
rm daemon.sh
rm configure.sh
wget https://bitbucket.org/nextwebua/imgs2video-v2-project/raw/345220e864ff7e225859ad54ee1e3d6f1b4638e0/daemon.sh
wget https://bitbucket.org/nextwebua/imgs2video-v2-project/raw/345220e864ff7e225859ad54ee1e3d6f1b4638e0/configure.sh
chmod 777 daemon.sh
chmod 777 configure.sh





if [ -z "$(ls -A $CLIENT_CONFIG_FOLDER)" ]; then
  echo "Config dir is empty"
  exit 1
fi

for f in $CLIENT_CONFIG_FILES; do
     PID_NAME=$(basename ${f%.*})
     echo "SOURCE FILE PATH $f"
     source $f

     #truncate logs
     truncate -s 0 $LOG_DIR/runtime.log
     truncate -s 0 $LOG_DIR/runtime_debug.log


      #webcam copy image
      wget --connect-timeout=2 --read-timeout=5 $URL -O 'webcam_test.jpg' 2>&1
      echo "Destination image"
      echo "$RSYNC_SSH_USERNAME@$MASTER_SERVER:/opt/i2v/$NAME/configure_preview.jpg"
      rsync -e "$RSYNC_E" $RSYNC_OPTS --archive --partial --verbose webcam_test.jpg "$RSYNC_SSH_USERNAME@$MASTER_SERVER:/opt/i2v/$NAME/configure_preview.jpg"

      if [[ $? -ne 0 ]]
      then
          echo "Test webcam image file was NOT uploaded to remote server"
          rm 'webcam_test.jpg'
          exit 1
      else
          echo "Test webcam image file SUCCESSFULLY UPLOADED TO REMOTE SERVER"
      fi

      rm 'webcam_test.jpg'

 done







