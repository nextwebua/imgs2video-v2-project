#!/bin/bash

FOLDER_PATH=/opt/i2v
for d in */ ; do
    if [[ "$d" == "imgs2video/" ]]; then
        continue
    fi


    NAME=${d%/}
    echo "Build for $NAME"
    IMGS2VIDEO_CFGFILE=${FOLDER_PATH}/${NAME}/config.i2v IMGS2VIDEO_OVERRIDECFGFILE=${FOLDER_PATH}/configureVariables.i2v nohup ${FOLDER_PATH}/imgs2video/build_all_f$
done