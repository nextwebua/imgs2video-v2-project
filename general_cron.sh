#!/bin/bash
FOLDER_PATH=/opt/i2v


for d in */ ; do
    sleep 30
    if [[ "$d" == "imgs2video/" ]]; then
        continue
    fi


    NAME=${d%/}
    echo `date`
    echo "Build for $NAME"
    echo "------"
    IMGS2VIDEO_CFGFILE=${FOLDER_PATH}/${NAME}/config.i2v IMGS2VIDEO_OVERRIDECFGFILE=${FOLDER_PATH}/configureVariables.i2v nohup ${FOLDER_PATH}/imgs2video/build_all_from_hours_and_remove_old.sh $NAME &> ${FOLDER_PATH}/${NAME}/log/build_all__`date +%F_%H_%M`.log
done