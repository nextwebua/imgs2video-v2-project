#!/bin/bash

if [[ -f "configureServerVariables.i2v" ]]
then
    CONFIG_PATH="configureServerVariables.i2v"
else
    CONFIG_PATH="configureServerVariablesSample.i2v"
fi

source $CONFIG_PATH

echo $CONFIG_PATH

echo "Configuration started. For EXIT Press CTRL + C"

echo "Remote Server IP [Default: $MASTER_SERVER]"
read NEW_MASTER_SERVER
if [[ -z "$NEW_MASTER_SERVER" ]]
then
    NEW_MASTER_SERVER=$MASTER_SERVER
fi


echo "Username on remote server"
read NEW_USERNAME
if [[ -z "$NEW_USERNAME" ]]
then
    echo "You should provide username to connect on the server. Exit...'"
    exit 1
fi

APP_BASE_PATH=`pwd`
SSH_FILE_NAME=$(echo ${APP_BASE_PATH//\//-}| cut -c 2-)
SSH_FILE_PATH=~/.ssh/$SSH_FILE_NAME

mkdir -p ~/.ssh

ssh-keygen -t rsa -N "" -f $SSH_FILE_PATH
if [[ -z $SSH_FILE_PATH ]]
then
    echo 'Something wrong, your keys do not setup proper way. Please try again as sudo user. Exit...'
    exit 1
fi

ssh-keygen -R $NEW_MASTER_SERVER

ssh-copy-id -i $SSH_FILE_PATH.pub -p $MASTER_SERVER_SSH_PORT $NEW_USERNAME@$NEW_MASTER_SERVER

if [[ $? -ne 0 ]]
then
    echo "Fail to upload key. Please check logs above. Exit...'"
    exit 1
fi

echo "Testing SSH Connection.."
ssh -q $NEW_USERNAME@$NEW_MASTER_SERVER -i $SSH_FILE_PATH -p $MASTER_SERVER_SSH_PORT exit
if [[ $? -ne 0 ]]
then
    echo "Can not connect to SSH after setup. Please check logs above. Exit...'"
    exit 1
fi
echo "SSH Connection - OK"
echo "Save config to the file - configureServerVariables.i2v"

echo "# For upload of hourly videos onto central remote server
HV_AND_LOG_SYNC=yes
# uncomment to enable infinite loop (in daemon) uploading new hourly videos and logs to central server
MASTER_SERVER=\"$NEW_MASTER_SERVER\"
MASTER_SERVER_SSH_PORT=22
RSYNC_SSH_USERNAME=\"$NEW_USERNAME\"
RSYNC_SSH_FILE_PATH=\"$SSH_FILE_PATH\"" > configureServerVariables.i2v

cat << 'EOF' >> configureServerVariables.i2v
RSYNC_E="ssh -i $RSYNC_SSH_FILE_PATH -p $MASTER_SERVER_SSH_PORT -o ConnectTimeout=10"
RSYNC_OPTS="--timeout 10"
SRC_DIR="$BASEPATH/$NAME"
DST_URI="$RSYNC_SSH_USERNAME@$MASTER_SERVER:/opt/i2v/$NAME"
EOF

echo "Server Configuration done"
exit 0