#!/bin/bash
if [[ -z $IMGS2VIDEO_CFGFILE ]]
then
    echo 'IMGS2VIDEO_CFGFILE env var must be available'
    exit 1
fi
if ! [[ -f $IMGS2VIDEO_CFGFILE ]]
then
    echo "Cfg file $IMGS2VIDEO_CFGFILE does not exist"
    exit 1
fi
source $IMGS2VIDEO_CFGFILE

DOMAIN_NAME="http://i2v.transship.ua"
JSON_FILE_PATH=$BASEPATH/$NAME/video_range.json
echo $DOMAIN_NAME/api/video/getRange/$NAME
wget --connect-timeout=5 --read-timeout=5 --tries=5 $DOMAIN_NAME/api/video/getRange/$NAME -O $JSON_FILE_PATH 2>&1
if [[ $? -ne 0 ]]
then
    echo "Server not available"
    exit 1
else
    JSON_CONTENT=$(cat $JSON_FILE_PATH)
fi

echo "CONTENT"
echo $JSON_CONTENT
#json content for test
# JSON_CONTENT=$(cat video_event.json)

if [ $JSON_CONTENT == "[]" ]
then
    echo "No event"
    exit 1
fi

MINUTE_PARTS=$(echo $JSON_CONTENT | jq -r '.parts[]')
DATE_FOLDER=$(echo $JSON_CONTENT | jq -r '.date')
VIDEO_ID=$(echo $JSON_CONTENT | jq -r '.id')

MERGE_OUTPUT_FOLDER="$BASEPATH/$NAME/video_range"
OUTPUT_FILE_PATH="$MERGE_OUTPUT_FOLDER/$VIDEO_ID.mp4"


mkdir -p $MERGE_OUTPUT_FOLDER

FFMPEG_MERGE_SOURCE_FILE="$BASEPATH/$NAME/minutes_ffmpeg_source.txt"
if [ -f $FFMPEG_MERGE_SOURCE_FILE ]
then
    rm $FFMPEG_MERGE_SOURCE_FILE
fi
touch $FFMPEG_MERGE_SOURCE_FILE

for VIDEO_PART_FILE in $MINUTE_PARTS; do

    FILE=$BASEPATH/$NAME/video_ms/$DATE_FOLDER/$VIDEO_PART_FILE
    echo $FILE

    if [ -f $FILE ]
    then
        echo "EXISTS"
        echo "file '$FILE'" >> $FFMPEG_MERGE_SOURCE_FILE
    fi
done


if [[ -z $(grep '[^[:space:]]' $FFMPEG_MERGE_SOURCE_FILE) ]]
then
    echo "Video parts is empty..."
    wget --connect-timeout=5 --read-timeout=5 --tries=5 $DOMAIN_NAME/api/video/rangeResponse/$VIDEO_ID -O /dev/null 2>&1
    #TODO - send curl
    exit 1
fi


FFMPEG_RUN_STATUS="fail"
$FFMPEG -f \
    concat \
    -safe 0 \
    -i $FFMPEG_MERGE_SOURCE_FILE \
    -c copy \
    -y $OUTPUT_FILE_PATH && FFMPEG_RUN_STATUS="ok"


if [[ $FFMPEG_RUN_STATUS == "ok" ]]
then
    echo "FILE Was Successfully Saved TO $OUTPUT_FILE_PATH"
else
    echo "ERROR - FILE $OUTPUT_FILE_PATH was note generate via FFMPEG"
    wget --connect-timeout=5 --read-timeout=5 --tries=5 $DOMAIN_NAME/api/video/rangeResponse/$VIDEO_ID -O /dev/null 2>&1
fi