#!/bin/bash

CLIENT_NAME=$1 # exporting for routines in sub-shells
OUTPUT_FILE_NAME="${CLIENT_NAME}.i2v"
OUTPUT_FILE_PATH="client_config/${OUTPUT_FILE_NAME}"

mkdir -p "client_config"

if ! [[ -f $OUTPUT_FILE_PATH ]]
then
  if [[ -f "config.i2v" ]]
  then
      CONFIG_PATH="config.i2v"
  else
      CONFIG_PATH="config-sample.i2v"
  fi
else
    echo "Edit config ${OUTPUT_FILE_PATH}"
    CONFIG_PATH=$OUTPUT_FILE_PATH
fi



source $CONFIG_PATH

#override to
NAME=$CLIENT_NAME

echo "Camera Folder name [Default: $NAME]"
read NEW_NAME
if [[ -n "$NEW_NAME" ]]
then
    NAME=$NEW_NAME
fi

if [[ -z "$NAME" ]]
then
   echo "Camera Folder name can't be empty"
   exit 1
fi

CLIENT_NAME=$NAME
OUTPUT_FILE_NAME="${CLIENT_NAME}.i2v"
OUTPUT_FILE_PATH="client_config/${OUTPUT_FILE_NAME}"



echo "Webcam URL [Default: $URL]"
read NEW_URL
if [[ -z "$NEW_URL" ]]
then
    NEW_URL=$URL
fi

wget --connect-timeout=2 --read-timeout=5 $NEW_URL -O 'webcam_test.jpg' 2>&1

if [[ $? -ne 0 ]]
then
    echo "`date` - Webcam URL failed for $NEW_URL, please try again"
    rm 'webcam_test.jpg'
    exit 1
else
    echo "Webcam URL - OK"
fi

echo "Sleep per every image in seconds [Default: $AFTER_GET_IMAGE_HOOK]"
read NEW_AFTER_GET_IMAGE_HOOK
if [[ -z "$NEW_AFTER_GET_IMAGE_HOOK" ]]
then
    NEW_AFTER_GET_IMAGE_HOOK=$AFTER_GET_IMAGE_HOOK
else
    NEW_AFTER_GET_IMAGE_HOOK="sleep $NEW_AFTER_GET_IMAGE_HOOK"
fi

echo "Notification Email [Default: $NOTIF_EMAILS]"
read NEW_NOTIF_EMAILS
if [[ -z "$NEW_NOTIF_EMAILS" ]]
then
    NEW_NOTIF_EMAILS=$NOTIF_EMAILS
fi


echo "Save IMAGE in days [Default: $SAVE_IMGS_DAYS]"
read NEW_SAVE_IMGS_DAYS
if [[ -z "$NEW_SAVE_IMGS_DAYS" ]]
then
    NEW_SAVE_IMGS_DAYS=$SAVE_IMGS_DAYS
fi


echo "Save Video HOURS in days [Default: $SAVE_VIDEO_HOURS_DAYS]"
read NEW_SAVE_VIDEO_HOURS_DAYS
if [[ -z "$NEW_SAVE_VIDEO_HOURS_DAYS" ]]
then
    NEW_SAVE_VIDEO_HOURS_DAYS=$SAVE_VIDEO_HOURS_DAYS
fi


echo "Save Video DAYS in days [Default: $SAVE_VIDEO_DAYS_DAYS]"
read NEW_SAVE_VIDEO_DAYS_DAYS
if [[ -z "$NEW_SAVE_VIDEO_DAYS_DAYS" ]]
then
    NEW_SAVE_VIDEO_DAYS_DAYS=$SAVE_VIDEO_DAYS_DAYS
fi


echo "Save LOGS in days [Default: $SAVE_LOG_DAYS]"
read NEW_SAVE_LOG_DAYS
if [[ -z "$NEW_SAVE_LOG_DAYS" ]]
then
    NEW_SAVE_LOG_DAYS=$SAVE_LOG_DAYS
fi

if [[ -f "configureServerVariables.i2v" ]]
then
    CONFIG_SERVER_PATH="configureServerVariables.i2v"
else
    CONFIG_SERVER_PATH="configureServerVariablesSample.i2v"
fi

source $CONFIG_SERVER_PATH


#echo "Name: $NEW_NAME"
#echo "Assets Path: $NEW_BASEPATH"
#echo "Notification Email: $NEW_NOTIF_EMAILS"
#echo "Webcam URL: $NEW_URL"

echo "Configuration done"


echo "NAME=\"$NAME\""  > "$OUTPUT_FILE_PATH"
cat << 'EOF' >> "$OUTPUT_FILE_PATH"
BASEPATH=`pwd`
EOF


echo "FFMPEG=\"$FFMPEG\"
FFPROBE=\"$FFPROBE\"
BITRATE=$BITRATE
SPEEDUP=$SPEEDUP
FRAMERATE=$FRAMERATE
#declare -A VIDEO_ENCODING_OPTS_ARRAY
VIDEO_ENCODING_OPTS_ARRAY[\"mp4\"]=\" -t 60 -vcodec libx264 -b:v \$BITRATE -bt:v \$(( BITRATE / 10 )) -preset medium -movflags faststart \"
#VIDEO_ENCODING_OPTS_ARRAY[\"webm\"]=\" -t 60 -vcodec vp8 -b:v \$BITRATE -bt:v \$(( BITRATE / 10 )) \"
FILTER=\"$FILTER\"
OFMTS=\"$OFMTS\" # output format
#NOCAT=yes # uncomment to omit concatenation and make only hourly video fragments" >> "$OUTPUT_FILE_PATH"


cat << 'EOF' >> "$OUTPUT_FILE_PATH"
IMGSDIR=$BASEPATH/$NAME/imgs_by_hour_dir
VIDEODIR=$BASEPATH/$NAME/video_hours
DAILY_VIDEO_DIR=$BASEPATH/$NAME/video_days
LOG_DIR=$BASEPATH/$NAME/log
DAYFILE=$BASEPATH/$NAME/last_day #prefix without extension
EOF



echo "SAVE_IMGS_DAYS=$NEW_SAVE_IMGS_DAYS
SAVE_VIDEO_HOURS_DAYS=$NEW_SAVE_VIDEO_HOURS_DAYS
SAVE_VIDEO_DAYS_DAYS=$NEW_SAVE_VIDEO_DAYS_DAYS
SAVE_LOG_DAYS=$NEW_SAVE_LOG_DAYS
URL=\"$NEW_URL\"
# one more nice URL: http://www.wirednewyork.com/images/webcams/wired-new-york-webcam3.jpg

AFTER_GET_IMAGE_HOOK='$NEW_AFTER_GET_IMAGE_HOOK'
AFTER_HOUR_PROC_HOOK='$AFTER_HOUR_PROC_HOOK'
DAILY_HOOK='$DAILY_HOOK'
NOTIF_EMAILS=\"$NEW_NOTIF_EMAILS\"" >> "$OUTPUT_FILE_PATH"

nano "$OUTPUT_FILE_PATH"

echo "Local config successfully saved to ${OUTPUT_FILE_PATH}"

echo "Do you want save file to the remote server ? [yes/no]"
read IS_SAVE

if [[ "$IS_SAVE" == "yes" ]]
then



  echo "Testing SSH Connection.."

  source $CONFIG_SERVER_PATH

  if ! [[ -f $RSYNC_SSH_FILE_PATH ]]
  then
    ./initd configure-server

    source $CONFIG_SERVER_PATH
  fi


  ssh -q $RSYNC_SSH_USERNAME@$MASTER_SERVER -i $RSYNC_SSH_FILE_PATH -p $MASTER_SERVER_SSH_PORT exit

  if [[ $? -ne 0 ]]
  then
      echo "Can not connect to SSH after setup. Please check logs above.'"
      echo "Run server configure mode..."
      ./initd configure-server

      #todo - check command code
      if [[ $? -ne 0 ]]
      then
        echo "Server configuration still not setup - please run sudo ./initd configure-server command"
        rm 'webcam_test.jpg'
        exit 1
      fi
  fi

  ssh  $RSYNC_SSH_USERNAME@$MASTER_SERVER -i $RSYNC_SSH_FILE_PATH -p $MASTER_SERVER_SSH_PORT "mkdir -p /opt/i2v/$NAME"

  rsync -e "$RSYNC_E" $RSYNC_OPTS --archive --partial --verbose "$OUTPUT_FILE_PATH" "$DST_URI/config.i2v"

  if [[ $? -ne 0 ]]
  then
      echo "Config File NOT UPLOADED TO REMOTE SERVER. Please reconfigure server config by run sudo ./initd configure-server"
      rm 'webcam_test.jpg'
      exit 1
  else
      echo "Config File SUCCESSFULLY UPLOADED TO REMOTE SERVER"
  fi



  rsync -e "$RSYNC_E" $RSYNC_OPTS --archive --partial --verbose webcam_test.jpg "$DST_URI/configure_preview.jpg"

  if [[ $? -ne 0 ]]
  then
      echo "Test webcam image file was NOT uploaded to remote server"
      exit 1
  else
      echo "Test webcam image file SUCCESSFULLY UPLOADED TO REMOTE SERVER"
  fi

  rm 'webcam_test.jpg'

fi