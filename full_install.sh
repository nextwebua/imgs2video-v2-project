#!/bin/bash

INSTALL_DIR="$(pwd)"/imgs2video
RUN_ON_START_PATH="/etc/rc.local"
STARTUP_CODE="cd $INSTALL_DIR && ./initd force-restart"
INITD_PATH="$INSTALL_DIR/initd"
SERVICE_PATH="/usr/bin/i2v"
CLIENT_CONFIG_DIR="$INSTALL_DIR/client_config"

apt-get update
#apt-get install
INSTALL_URL="https://bitbucket.org/nextwebua/imgs2video-v2-project/get/master.zip"
INSTALL_ARCHIVE_NAME="install.zip"


wget $INSTALL_URL -O $INSTALL_ARCHIVE_NAME 2>&1
if [[ $? -ne 0 ]]
then
    echo "Fail to download install file FROM $INSTALL_URL"
    rm $INSTALL_ARCHIVE_NAME
    exit 1
fi

echo "Checking packages...."
if hash unzip 2>/dev/null; then
    echo "Unzip installed...."
else
    apt-get install -y unzip
fi


if hash wget 2>/dev/null; then
    echo "Unzip installed...."
else
    apt-get install -y wget
fi

if hash nano 2>/dev/null; then
    echo "Nano installed...."
else
    apt-get install -y nano
fi

if hash jq 2>/dev/null; then
    echo "JQ installed...."
else
    apt-get install -y jq
fi

apt-get install -y libvpx
apt-get install -y libvpx-dev

apt-get install -y cmake make g++ perl

apt-get install -y libgmp10
apt-get install -y libgmp-dev
apt-get install -y libx264-dev
apt-get install -y libx265-dev
apt-get install -y ntp
apt-get install -y imagemagick


UNZIP_OUTPUT="$(unzip -o $INSTALL_ARCHIVE_NAME)"
if [[ $? -ne 0 ]]
then
    echo "Unzip fail\n $UNZIP_OUTPUT"
    rm $INSTALL_ARCHIVE_NAME
    exit 1
fi

EXTRACT_FOLDER_NAME=$(unzip -l install.zip | awk '/\/$/ { print $NF }'| head -n 1)
mv $EXTRACT_FOLDER_NAME imgs2video


rm install.zip



chmod 777 imgs2video/build.sh
RD=$(pwd)

cd imgs2video
./build.sh
cd $RD || exit


BUILD_FFPATH="imgs2video/ffmpeg"


if ! [[ -f $BUILD_FFPATH ]]
then
    echo "FFMPEG Build failed - please check logs"
    exit 1
fi

rm -rf imgs2video/buildlib/*
rm -rf imgs2video/external/ffmpeg/install/bin/
mv imgs2video/ffmpeg imgs2video/buildlib/ffmpeg
mv imgs2video/ffprobe imgs2video/buildlib/ffprobe


mkdir -p $CLIENT_CONFIG_DIR
chmod 777 $CLIENT_CONFIG_DIR


chmod 777 imgs2video/initd
chmod 777 imgs2video/configure.sh
chmod 777 imgs2video/configure_server.sh
chmod 777 imgs2video/configure_preview.sh
chmod 777 imgs2video/daemon.sh
chmod 777 imgs2video/cat.sh
chmod 777 imgs2video/enableAutoRun.sh
chmod 777 imgs2video/buildlib/ffmpeg
chmod 777 imgs2video/buildlib/ffprobe
chmod 777 imgs2video/dashboard.sh
chmod 777 imgs2video/configValidate.sh
chmod 777 imgs2video/merge_minute_range_videos.sh
chmod 777 imgs2video/make_minutes_video.sh
chmod 777 imgs2video/minutes_video_request.sh
chmod 777 imgs2video/install_jetson_ffmpeg.sh


if ! [[ -f $RUN_ON_START_PATH ]]
then
echo "Create startup file"
touch $RUN_ON_START_PATH
chmod +x $RUN_ON_START_PATH
cat << 'EOF' >> "$RUN_ON_START_PATH"
#!/bin/bash
EOF
fi

if ! grep -Fxq "$STARTUP_CODE" $RUN_ON_START_PATH
then
echo "$STARTUP_CODE" >> "$RUN_ON_START_PATH"
echo "Startup successfully installed to $RUN_ON_START_PATH"
fi

echo "Add software as a service..."

if [[ -f $SERVICE_PATH ]]
then
    rm $SERVICE_PATH
fi

ln -s $INITD_PATH $SERVICE_PATH
if [[ $? -ne 0 ]]
then
    echo "Sofrware as a service install was failed, but you still can run it from the initd file"
else
    echo "Service was successfully added. Use i2v command from any place in your system to run the software"
fi


#add to auto run
echo "Install done successfully"

#read -p 'Do you have run configuration process ? [y/n]: ' IS_START_CONFIGURE

#if [[ "$IS_START_CONFIGURE" = "y" ]]
#then
#    cd imgs2video
#    ./configure.sh
#else
#    echo "If you need configuration please run it manually"
#fi