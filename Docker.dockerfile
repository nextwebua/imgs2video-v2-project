FROM ubuntu:18.04

#Fix iconv(): Wrong charset, conversion from `UTF-8' to `UTF-8//TRANSLIT' is not allowed
#@see https://github.com/php-earth/docker-php/issues/3
#RUN apk add --no-cache --repository http://dl-cdn.alpinelinux.org/alpine/edge/community gnu-libiconv
#ENV LD_PRELOAD /usr/lib/preloadable_libiconv.so php

RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get install -y wget cmake make g++ perl libgmp10 libgmp-dev libvpx-dev libx264-dev libx265-dev \
    && apt-get install -y htop nano tmux screen\
    && apt-get install -y rsync ssh jq\
    && rm -rf /tmp/* /var/cache/apk/*


# run copy all files
#COPY ./ /var/www/html/

COPY ./ /var/www/html/docker.sh

WORKDIR /var/www/html/

#run build ffmpeg and keep-alive
CMD ["/var/www/html/docker.sh"]