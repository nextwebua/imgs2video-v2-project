#!/bin/bash

CLIENT_NAME=$1 # exporting for routines in sub-shellsz
CONFIG_NAME="${CLIENT_NAME}.i2v"
CONFIG_PATH="client_config/${CONFIG_NAME}"

if ! [[ -f $CONFIG_PATH ]]
then
    echo "Config for $CLIENT_NAME not found"
    exit 1
fi

if [[ -f "configureServerVariables.i2v" ]]
then
    CONFIG_SERVER_PATH="configureServerVariables.i2v"
else
    CONFIG_SERVER_PATH="configureServerVariablesSample.i2v"
fi


source $CONFIG_PATH

echo "Testing SSH Connection.."

source $CONFIG_SERVER_PATH

if ! [[ -f $CONFIG_SERVER_PATH ]]
then
  ./initd configure-server

  source $CONFIG_SERVER_PATH
fi

echo "Z"

ssh -q $RSYNC_SSH_USERNAME@$MASTER_SERVER -i $RSYNC_SSH_FILE_PATH -p $MASTER_SERVER_SSH_PORT exit

if [[ $? -ne 0 ]]
then
  echo "Can not connect to SSH after setup. Please check logs above.'"
  echo "Run server configure mode..."
  ./initd configure-server
fi

wget --connect-timeout=2 --read-timeout=5 $URL -O 'webcam_test.jpg' 2>&1

if [[ $? -ne 0 ]]
then
    echo "`date` - Webcam URL failed for $URL, please try again"
    rm 'webcam_test.jpg'
    exit 1
else
    echo "Webcam URL - OK"
fi


ssh  $RSYNC_SSH_USERNAME@$MASTER_SERVER -i $RSYNC_SSH_FILE_PATH -p $MASTER_SERVER_SSH_PORT "mkdir -p /opt/i2v/$NAME"

rsync -e "$RSYNC_E" $RSYNC_OPTS --archive --partial --verbose webcam_test.jpg "$DST_URI/configure_preview.jpg"

if [[ $? -ne 0 ]]
then
  echo "Test webcam image file was NOT uploaded to remote server"
  exit 1
else
  echo "Test webcam image file SUCCESSFULLY UPLOADED TO REMOTE SERVER to $DST_URI/configure_preview.jpg"
  echo "[$UTC_NOW][`date`][camera][preview] Превью камеры успешно инициализировано" >> "$LOG_DIR/monitoring.log"
fi

rm 'webcam_test.jpg'

