#!/bin/bash


if [[ -f "config.i2v" ]]
then
    CONFIG_PATH="config.i2v"
else
    CONFIG_PATH="config-sample.i2v"
fi

source $CONFIG_PATH

echo "Camera Folder name [Default: $NAME]"
read NEW_NAME
if [[ -z "$NEW_NAME" ]]
then
    NEW_NAME=$NAME
fi

echo "Webcam URL [Default: $URL]"
read NEW_URL
if [[ -z "$NEW_URL" ]]
then
    NEW_URL=$URL
fi

echo "Sleep per every image in seconds [Default: $AFTER_GET_IMAGE_HOOK]"
read NEW_AFTER_GET_IMAGE_HOOK
if [[ -z "$NEW_AFTER_GET_IMAGE_HOOK" ]]
then
    NEW_AFTER_GET_IMAGE_HOOK=AFTER_GET_IMAGE_HOOK
else
    NEW_AFTER_GET_IMAGE_HOOK="sleep $NEW_AFTER_GET_IMAGE_HOOK"
fi

echo "Image Dir [Default: $BASEPATH]"
read NEW_BASEPATH
if [[ -z "$NEW_BASEPATH" ]]
then
    NEW_BASEPATH="$BASEPATH"
fi

echo "Notification Email [Default: $NOTIF_EMAILS]"
read NEW_NOTIF_EMAILS
if [[ -z "$NEW_NOTIF_EMAILS" ]]
then
    NEW_NOTIF_EMAILS=$NOTIF_EMAILS
fi


echo "Save IMAGE in days [Default: $SAVE_IMGS_DAYS]"
read NEW_SAVE_IMGS_DAYS
if [[ -z "$NEW_SAVE_IMGS_DAYS" ]]
then
    NEW_SAVE_IMGS_DAYS=$SAVE_IMGS_DAYS
fi


echo "Save Video HOURS in days [Default: $SAVE_VIDEO_HOURS_DAYS]"
read NEW_SAVE_VIDEO_HOURS_DAYS
if [[ -z "$NEW_SAVE_VIDEO_HOURS_DAYS" ]]
then
    NEW_SAVE_VIDEO_HOURS_DAYS=$SAVE_VIDEO_HOURS_DAYS
fi


echo "Save Video DAYS in days [Default: $SAVE_VIDEO_DAYS_DAYS]"
read NEW_SAVE_VIDEO_DAYS_DAYS
if [[ -z "$NEW_SAVE_VIDEO_DAYS_DAYS" ]]
then
    NEW_SAVE_VIDEO_DAYS_DAYS=$SAVE_VIDEO_DAYS_DAYS
fi


echo "Save LOGS in days [Default: $SAVE_LOG_DAYS]"
read NEW_SAVE_LOG_DAYS
if [[ -z "$NEW_SAVE_LOG_DAYS" ]]
then
    NEW_SAVE_LOG_DAYS=$SAVE_LOG_DAYS
fi

source configureVariables.i2v

#echo "Name: $NEW_NAME"
#echo "Assets Path: $NEW_BASEPATH"
#echo "Notification Email: $NEW_NOTIF_EMAILS"
#echo "Webcam URL: $NEW_URL"

echo "Configuration done"


echo "NAME=\"$NEW_NAME\"
BASEPATH=\"$NEW_BASEPATH\"

FFMPEG=$FFMPEG
FFPROBE=$FFPROBE
BITRATE=$BITRATE
SPEEDUP=$SPEEDUP
FRAMERATE=$FRAMERATE
declare -A VIDEO_ENCODING_OPTS_ARRAY
VIDEO_ENCODING_OPTS_ARRAY[\"mp4\"]=\" -t 60 -vcodec libx264 -b:v \$BITRATE -bt:v \$(( BITRATE / 10 )) -preset medium -movflags faststart \"
#VIDEO_ENCODING_OPTS_ARRAY[\"webm\"]=\" -t 60 -vcodec vp8 -b:v \$BITRATE -bt:v \$(( BITRATE / 10 )) \"
FILTER=\"$FILTER\"
OFMTS=\"$OFMTS\" # output format
#NOCAT=yes # uncomment to omit concatenation and make only hourly video fragments

IMGSDIR=\"$IMGSDIR\"
VIDEODIR=\"$VIDEODIR\"
DAILY_VIDEO_DIR=\"$DAILY_VIDEO_DIR\"
LOG_DIR=\"$LOG_DIR\"
SAVE_IMGS_DAYS=$NEW_SAVE_IMGS_DAYS
SAVE_VIDEO_HOURS_DAYS=$NEW_SAVE_VIDEO_HOURS_DAYS
SAVE_VIDEO_DAYS_DAYS=$NEW_SAVE_VIDEO_DAYS_DAYS
SAVE_LOG_DAYS=$NEW_SAVE_LOG_DAYS
DAYFILE=\"$DAYFILE\" #prefix without extension
URL=\"$NEW_URL\"
# one more nice URL: http://www.wirednewyork.com/images/webcams/wired-new-york-webcam3.jpg

AFTER_GET_IMAGE_HOOK='$NEW_AFTER_GET_IMAGE_HOOK'
AFTER_HOUR_PROC_HOOK='$AFTER_HOUR_PROC_HOOK'
DAILY_HOOK='$DAILY_HOOK'
NOTIF_EMAILS=\"$NEW_NOTIF_EMAILS\"

# For upload of hourly videos onto central remote server
#HV_AND_LOG_SYNC=yes # uncomment to enable infinite loop (in daemon) uploading new hourly videos and logs to central server
MASTER_SERVER=\"$MASTER_SERVER\"
MASTER_SERVER_SSH_PORT=$MASTER_SERVER_SSH_PORT
RSYNC_E=\"$RSYNC_E\"
RSYNC_OPTS=\"$RSYNC_OPTS\"
SRC_DIR=\"$SRC_DIR\"
DST_URI=\"$DST_URI\"" > config.i2v

nano config.i2v

echo "Do you want save file to the remote server ? [yes/no]"
read IS_SAVE
if [[ "$IS_SAVE" == "yes" ]]
then
  rsync -e "$RSYNC_E" $RSYNC_OPTS --archive --partial --verbose "$BASEPATH/config.i2v" "$DST_URI/"
fi