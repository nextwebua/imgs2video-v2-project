#!/bin/bash

if [[ $# -ne 2 ]]
then
    echo "Usage: $0 <config file> <output yes/no>"
    echo "Config file should be derived from config-sample.i2v"
    exit 1
fi

function addError {
  export CONFIG_STATUS="fail"
  CONFIG_ERRORS=( "${CONFIG_ERRORS[@]}" "$1" )
}

IMGS2VIDEO_CFGFILE=$1 # exporting for routines in sub-shells
PRINT_OUTPUT=$2

if ! [[ -f $IMGS2VIDEO_CFGFILE ]]
then
    echo "Cfg file $IMGS2VIDEO_CFGFILE does not exist"
    exit 1
fi

source $IMGS2VIDEO_CFGFILE

declare -a CONFIG_ERRORS
CONFIG_STATUS="ok"

if [[ $NAME == "" ]]
then
  addError "NAME variable is empty"
fi

if [[ $BASEPATH == "" ]]
then
  addError "BASEPATH variable is empty"
fi

if ! [[ -d $BASEPATH ]] && [[ $BASEPATH != "" ]]
then
  addError "BASEPATH $BASEPATH doesnt exists"
fi

if [[ -d $BASEPATH ]] && [[ $BASEPATH != "" ]] && ! [[ -f "$BASEPATH/daemon.sh" ]]
then
  addError "BASEPATH $BASEPATH exists but there no daemon.sh file"
fi

if [[ $FFMPEG == "" ]]
then
  addError "FFMPEG variable is empty"
fi

if ! [[ -f $FFMPEG ]] && [[ $FFMPEG != "" ]]
then
  addError "FFMPEG path doesnt exists $FFMPEG"
fi

if [[ $FFPROBE == "" ]]
then
  addError "FFPROBE variable is empty"
fi

if ! [[ -f $FFPROBE ]] && [[ $FFPROBE != "" ]]
then
  addError "FFPROBE path doesnt exists $FFPROBE"
fi


if [[ $BITRATE == "" ]]
then
  addError "BITRATE variable is empty"
fi

if [[ $SPEEDUP == "" ]]
then
  addError "SPEEDUP variable is empty"
fi

if [[ $FRAMERATE == "" ]]
then
  addError "FRAMERATE variable is empty"
fi

if [[ $FILTER == "" ]]
then
  addError "FILTER variable is empty"
fi

if [[ $OFMTS == "" ]]
then
  addError "OFMTS variable is empty"
fi

if [[ $IMGSDIR == "" ]]
then
  addError "IMGSDIR variable is empty"
fi

if [[ $VIDEODIR == "" ]]
then
  addError "VIDEODIR variable is empty"
fi

if [[ $DAILY_VIDEO_DIR == "" ]]
then
  addError "DAILY_VIDEO_DIR variable is empty"
fi

if [[ $LOG_DIR == "" ]]
then
  addError "LOG_DIR variable is empty"
fi

if [[ $DAYFILE == "" ]]
then
  addError "DAYFILE variable is empty"
fi

if [[ $SAVE_IMGS_DAYS == "" ]]
then
  addError "SAVE_IMGS_DAYS variable is empty"
fi

if [[ $SAVE_VIDEO_HOURS_DAYS == "" ]]
then
  addError "SAVE_VIDEO_HOURS_DAYS variable is empty"
fi

if [[ $SAVE_VIDEO_DAYS_DAYS == "" ]]
then
  addError "SAVE_VIDEO_DAYS_DAYS variable is empty"
fi

if [[ $SAVE_LOG_DAYS == "" ]]
then
  addError "SAVE_LOG_DAYS variable is empty"
fi

if [[ $URL == "" ]]
then
  addError "URL variable is empty"
fi

if [ -z ${AFTER_GET_IMAGE_HOOK+x} ]
then
  addError "AFTER_GET_IMAGE_HOOK variable is not set"
fi

if [ -z ${AFTER_HOUR_PROC_HOOK+x} ]
then
  addError "AFTER_HOUR_PROC_HOOK variable is not set"
fi

if [ -z ${DAILY_HOOK+x} ]
then
  addError "DAILY_HOOK variable is not set"
fi

if [ -z ${NOTIF_EMAILS+x} ]
then
  addError "NOTIF_EMAILS variable is not set"
fi


#check FFMPEG BUILD
if [[ -f $FFMPEG ]] && [[ $FFMPEG != "" ]] && [[ $URL != "" ]]
then

  mkdir -p "testdir"
  wget -q -O testdir/1.jpg $URL
  #predefine as fail
  FFMPEG_STATUS="fail"


    if [[ -z $IS_VIDEO_ENCODING ]] && [[ $IS_VIDEO_ENCODING == "YES" ]]
    then
        cat  "testdir/*.jpg" | $FFMPEG -f image2pipe -r $FRAMERATE -i - $VIDEO_ENCODING_OPTS -loglevel 0 -y "testdir/t.mp4" && FFMPEG_STATUS="ok"
    else
          $FFMPEG \
          -r $FRAMERATE \
          -f image2 \
          -pattern_type glob \
          -i "testdir/*.jpg" \
          ${VIDEO_ENCODING_OPTS_ARRAY[$OFMTS]} \
          -loglevel 0 \
          -y \
          "testdir/t.mp4" && FFMPEG_STATUS="ok"
    fi


  if [[ $FFMPEG_STATUS == "" ]]
  then
    FFMPEG_STATUS="fail"
    addError "FFMPEG_STATUS - FFMPEG command is broken"
  fi

else
  FFMPEG_STATUS="fail"
fi

if [[ -d "testdir" ]]
then
  rm -rf "testdir"
fi


#yes/no
if [[ $PRINT_OUTPUT == "yes" ]]
then
  printf '%s\n' "${CONFIG_ERRORS[@]}"
else
  echo "CONFIG_STATUS=$CONFIG_STATUS FFMPEG_STATUS=$FFMPEG_STATUS"
fi

#ok/fail
if [[ $CONFIG_STATUS == "ok" ]]
then
  exit 0
else
  exit 1
fi




