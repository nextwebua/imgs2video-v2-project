#!/bin/bash

INSTALL_DIR="$(pwd)"/imgs2video
UPGRADE_DIR="$(pwd)"
RUN_ON_START_PATH="/etc/rc.local"
STARTUP_CODE="cd $INSTALL_DIR && ./initd force-restart"
INITD_PATH="$INSTALL_DIR/initd"
SERVICE_PATH="/usr/bin/i2v"
CLIENT_CONFIG_DIR="$INSTALL_DIR/client_config"

SHELL_FILES="${INSTALL_DIR}/*.sh"


echo "Before Install checks..."
if [[ -d $INSTALL_DIR ]]
then
    echo "Directory $INSTALL_DIR already exists. Please remove it or move to different folder for installation"
    exit 1
fi
echo "Before Install checks... OK"


echo "Stop Service"
./initd stop


apt-get update
#apt-get install
INSTALL_URL="https://bitbucket.org/nextwebua/imgs2video-v2-project/get/master.zip"
INSTALL_ARCHIVE_NAME="install.zip"

wget $INSTALL_URL -O $INSTALL_ARCHIVE_NAME 2>&1
if [[ $? -ne 0 ]]
then
    echo "Fail to download install file FROM $INSTALL_URL"
    rm $INSTALL_ARCHIVE_NAME
    exit 1
fi

echo "Checking packages...."
if hash unzip 2>/dev/null; then
    echo "Unzip installed...."
else
    apt-get install -y unzip
fi


if hash wget 2>/dev/null; then
    echo "Unzip installed...."
else
    apt-get install -y wget
fi

if hash nano 2>/dev/null; then
    echo "Nano installed...."
else
    apt-get install -y nano
fi

if hash jq 2>/dev/null; then
    echo "JQ installed...."
else
    apt-get install -y jq
fi

apt-get install -y libvpx
apt-get install -y libvpx-dev

apt-get install -y cmake make g++ perl

apt-get install -y libgmp10
apt-get install -y libgmp-dev
apt-get install -y libx264-dev
apt-get install -y libx265-dev
apt-get install -y ntp
apt-get install -y imagemagick


UNZIP_OUTPUT="$(unzip -o $INSTALL_ARCHIVE_NAME)"
if [[ $? -ne 0 ]]
then
    echo "Unzip fail\n $UNZIP_OUTPUT"
    rm $INSTALL_ARCHIVE_NAME
    exit 1
fi

EXTRACT_FOLDER_NAME=$(unzip -l install.zip | awk '/\/$/ { print $NF }'| head -n 1)
mv $EXTRACT_FOLDER_NAME imgs2video


rm install.zip

echo "Start file replacement"

for f in $SHELL_FILES; do
    FILE_NAME=$(basename "$f")

    if [ $FILE_NAME != "upgrade.sh" ]
    then
        if [ -f $FILE_NAME ]
        then
            rm $FILE_NAME
        fi

        cp imgs2video/$FILE_NAME $FILE_NAME
        chmod 777 $FILE_NAME

        echo "FILE $FILE_NAME was replaced to a new version"        
    fi
done

rm initd
cp imgs2video/initd initd
chmod 777 initd


rm -rf imgs2video

# echo "Add software as a service..."

# if [[ -f "$SERVICE_PATH" ]]
# then
#     rm $SERVICE_PATH
# fi

# ln -s $SERVICE_PATH $UPGRADE_DIR/initd 
# if [[ $? -ne 0 ]]
# then
#     echo "Sofrware as a service install was failed, but you still can run it from the initd file"
# else
#     echo "Service was successfully added. Use i2v command from any place in your system to run the software"
# fi

echo "Upgrade done successfully"

if [[ -f "configureServerVariables.i2v" ]]
then
    echo "Config file found - start to test SSH connection"
    CONFIG_SERVER_PATH="configureServerVariables.i2v"
    source $CONFIG_SERVER_PATH    
    ssh -q $RSYNC_SSH_USERNAME@$MASTER_SERVER -i $RSYNC_SSH_FILE_PATH -p $MASTER_SERVER_SSH_PORT exit

    if [[ $? -ne 0 ]]
    then
        echo "Can not connect to SSH after setup. Please check logs above.'"
        echo "Run server configure mode..."
        ./initd configure-server

        #todo - check command code
        if [[ $? -ne 0 ]]
        then
            echo "Server configuration still not setup - please run sudo ./initd configure-server command"
            rm 'webcam_test.jpg'
            exit 1
        fi
    else
        echo "Server configuration - OK"
    fi
else
    echo "We should run configure server"
    ./initd configure-server
fi
