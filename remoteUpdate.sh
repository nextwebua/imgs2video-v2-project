#!/bin/bash

CONFIGURE_SERVER_VARIABLES='configureServerVariables.i2v'
if ! [[ -f $CONFIGURE_SERVER_VARIABLES ]]
then
    echo "Server config file $CONFIGURE_SERVER_VARIABLES does not exist"
    exit 1
fi

if [[ $# -ne 1 ]]
then
    echo "Usage: $0 <config file>"
    echo "Config file should be derived from config-sample.i2v"
    exit 1
fi

IMGS2VIDEO_CFGFILE=$1 # exporting for routines in sub-shells

if ! [[ -f $IMGS2VIDEO_CFGFILE ]]
then
    echo "Cfg file $IMGS2VIDEO_CFGFILE does not exist"
    exit 1
fi

source $IMGS2VIDEO_CFGFILE
source $CONFIGURE_SERVER_VARIABLES

BASEPATH=`pwd`
mkdir -p "$BASEPATH/client_config_dmp"

TEMP_CONFIG_FILENAME="client_config_dmp/temp_config_$NAME.i2v"

#wget --connect-timeout=2 --read-timeout=5 $CONFIG_UPDATE_URL -O $TEMP_CONFIG_FILENAME 2>&1

echo "Backup current file as $TEMP_CONFIG_FILENAME file"

if [[ -f "$TEMP_CONFIG_FILENAME" ]]
then
    rm $TEMP_CONFIG_FILENAME
fi

cp $IMGS2VIDEO_CFGFILE $TEMP_CONFIG_FILENAME
rm $IMGS2VIDEO_CFGFILE


rsync -e "$RSYNC_E" $RSYNC_OPTS --archive --partial --verbose "$DST_URI/config.i2v" "$IMGS2VIDEO_CFGFILE"
if [[ $? -ne 0 ]]
then
    cp $TEMP_CONFIG_FILENAME $IMGS2VIDEO_CFGFILE
    echo "Rsync failed, please check server config file"
    exit 1
fi

echo "Config Downloaded successfully"
echo "Start Config Validation"
./configValidate.sh $IMGS2VIDEO_CFGFILE no
if [[ $? -ne 0 ]]
then
  UTC_NOW=`date -u +%s`
  echo "[$UTC_NOW][`date`][server][remoteUpdate] Ошибка удаленного обновления с сервера: $(./configValidate.sh $IMGS2VIDEO_CFGFILE yes)" >> "$LOG_DIR/monitoring.log"
  echo -e "\e[31mUpdate failed\e[0m, log added, config recovered to the latest workable config version"
  echo "------Error Report Start------"
  ./configValidate.sh $IMGS2VIDEO_CFGFILE yes
  echo "------Error Report End------"

  echo "Recovery started"
  rm $IMGS2VIDEO_CFGFILE
  cp $TEMP_CONFIG_FILENAME $IMGS2VIDEO_CFGFILE

  if [[ -f $IMGS2VIDEO_CFGFILE ]]
  then
    echo -e "\e[92mRecovery Completed\e[0m"
  fi
  exit 1
fi


echo -e "\e[92mUpdate Successfully Completed.\e[0m You can find old config in the file $TEMP_CONFIG_FILENAME"



